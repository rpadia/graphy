import celery


class AsyncTask(celery.Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print('{0!r} failed here: {1!r}'.format(task_id, exc))
