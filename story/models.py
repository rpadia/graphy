import uuid

from django.db import models
from mongoengine import Document, fields, EmbeddedDocument
# Create your models here.


class DeviceInfo(EmbeddedDocument):
    model = fields.StringField()
    language = fields.StringField()
    user_agent = fields.StringField()
    manufacturer = fields.StringField()
    versionRelease = fields.StringField()
    appVersionCode = fields.StringField()
    latitude = fields.DecimalField(precision=9, round=6)
    longitude = fields.DecimalField(precision=9, round=6)
    app_name = fields.StringField()


class VideoPath(EmbeddedDocument):
    low_resolution = fields.StringField(required=False)
    medium_resolution = fields.StringField(required=False)
    best_resolution = fields.StringField(required=False)
    

class Story(Document):
    
    STORY_TYPE = {
        0: 'Image',
        1: 'Video',
        2: 'Text'
    }
    name = fields.StringField(required=True, default='')
    filename = fields.StringField(default='')
    original_path = fields.StringField(default='')
    # _file = fields.ObjectIdField()
    resized_image_path = fields.StringField(required=False)
    resized_video_path = fields.EmbeddedDocumentField(VideoPath)
    description = fields.StringField(required=False,default='')
    duration = fields.IntField(blank=True, null=True)
    _type = fields.IntField(max_length=1, choices=STORY_TYPE)
    device_info = fields.EmbeddedDocumentField(DeviceInfo)
    created_at = fields.DateTimeField()
    created_by = fields.StringField(required=True)
    active = fields.BooleanField(default=True)
    
    # objects = models.Manager()
    
    # def __repr__(self):
    #     return str(self.id)
        
    def __str__(self):
        return '{} - {} - {}'.format(self.name, self._type, self.created_by)
