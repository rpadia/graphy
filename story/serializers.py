from rest_framework import serializers
from rest_framework.fields import empty
from datetime import date, datetime
import re


class StoryGETSerializer(serializers.Serializer):
    
    id = serializers.CharField(required=True, allow_blank=False)
    name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    description = serializers.CharField(required=True, allow_blank=False, max_length=100)
    story_type_text = serializers.CharField(required=True, allow_blank=False, max_length=100)
    story_type = serializers.IntegerField(required=True)
    duration = serializers.DecimalField(default=0.0, max_digits=4, decimal_places=2)
    created = serializers.DictField(required=True)
    location = serializers.DictField(required=True)
