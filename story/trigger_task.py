import celery
import json
from celery.utils.log import get_task_logger
from .tasks import AsyncTask
from graphy import celery_app
from celery import task

from .trigger import Triggers

logger = get_task_logger(__name__)


@celery_app.task(base=AsyncTask, bind=True, max_retries=3)
def invoke_triggers(self, target, action, **kwargs):
    try:
        Triggers.trigger(target, action, **kwargs)
    except Exception as e:
        if self.request.retries >= self.max_retries:
            logger.error('Triggers failed for {}-{}. Parameters are {}'.format(
                target, action, json.dumps({'locals': locals()}, default=str)))
        else:
            self.retry(countdown=10, target=target, action=action, **kwargs)

