from rest_framework import routers as merouters

from .views import StoryViewSet

router = merouters.SimpleRouter(trailing_slash=True)
router.register('story', StoryViewSet, 'story')

urlpatterns = []
urlpatterns.extend(router.urls)
