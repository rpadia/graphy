#!/usr/bin/env python
"""
This module serves as the base module for a trigger based
framework, wherein events spanning the entirety of a project
can be captured and their corresponding handlers be called
accordingly.

This trigger based framework supports the following:
    -> Subscribe for event(s)
    -> Unsubscribe for event(s)

Attributes:
    -> target: identifies the resource on which the
                event has happened.
    -> action: identifies what event has happened.
    -> handler: identifies the handler to be invoked
                upon firing of the event. The handler
                should be a callable.

Subscription config:
{
    'target':{
        'action': {
            'handler': handler_reference
        }
    }
}
We can extend the config to add more triggers.

Usage:
    Triggers.subscribe('target', 'action', handler)
    Triggers.unsubscribe('target', 'action', handler)
    Triggers.trigger('target', 'action', **kwargs)
"""


class Triggers:
    """
    A singleton class to manage the entire
    lifecycle of a trigger.
    """

    __instance = None
    _subscription_dict = dict()

    @staticmethod
    def _get_instance():
        """
        Get instance of the class
        """

        if Triggers.__instance is None:
            Triggers()
        return Triggers.__instance

    def __init__(self):
        """
        Initialise class instance
        """

        if Triggers.__instance is not None:
            pass
        Triggers.__instance = self

    @classmethod
    def subscribe(cls, target, action, handler):
        """
        Method to subscribe for a trigger and register
        a handler for it.
        """
        self = cls._get_instance()

        _target = self._subscription_dict.get(target)
        if not isinstance(_target, dict):
            _target = self._subscription_dict[target] = {}

        _action = _target.get(action)
        if not isinstance(_action, dict):
            _action = _target[action] = {}

        if _action.get(str(handler)):
            return "Handler already subscribed to the "\
                    "requested target and action."
        elif not callable(handler):
            return 'Invalid handler, please pass a callable'
        else:
            _action[str(handler)] = handler

    @classmethod
    def unsubscribe(cls, target, action, handler):
        """
        Method to unsubscribe a handler for a trigger.
        """

        self = cls._get_instance()

        try:
            self._subscription_dict.get(target, {}).\
                get(action, {}).pop(str(handler))
        except KeyError:
            return "Specified handler does not exist for "\
                  "the given target and action."

    @classmethod
    def trigger(cls, target, action, **kwargs):
        """
        Method to trigger the handlers for the specified target and action.
        """

        self = cls._get_instance()
        handler_list = self._subscription_dict.get(target, {}).get(action, {})

        for i in handler_list.values():
            i(**kwargs)

        return "Executed handlers for {}/{}.".format(target, action)
