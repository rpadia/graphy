import uuid
from datetime import datetime
from bson import ObjectId

from ..models import Story, DeviceInfo
from ..utils import STORY_TYPE_MAPPER
from ..services.file_services import LocalStorageService
from ..trigger_task import invoke_triggers


class StoryStorer(object):
    '''
    Story Service to perform different actions on Stories
    '''
    def __init__(self, name=None, description=None, _type=None):
        self.name = name
        self.description = description
        self._type = _type
    
    @staticmethod
    def get_stories():
        '''
        :return: List of stories ordered in descending order of time
        '''
        response = {'data': []}
        stories = Story.objects.filter().order_by('created_by')
        for story in stories:
            story_details = {
                'id': str(story.id),
                'name': story.name,
                'description': story.description,
                'story_type': STORY_TYPE_MAPPER.get(int(story._type)),
                'duration': story.duration or 0,
                'created': {'by': story.created_by, 'at': story.created_at.strftime("%B %d, %Y")},
                'location': {'latitude': story.device_info.latitude,
                             'longitude': story.device_info.longitude},
                
            }
            response['data'].append(story_details)
        return 'success', 'Success', response
    
    @staticmethod
    def get_story(story_id=None):
        '''
        :param story_id:
        :return: Returns story on basis of story id
        '''
        response = {}
        if not story_id:
            return 'error', 'Please specify a story', response
        story = Story.objects.filter(id=story_id).first()
        response = {
            'id': story_id,
            'name': story.name,
            'description': story.description,
            'story_type_text': STORY_TYPE_MAPPER.get(int(story._type)),
            'story_type': int(story._type),
            'duration': story.duration or 0,
            'created': {'by': story.created_by, 'at': story.created_at},
            'location': {'latitude': story.device_info.latitude, 'longitude': story.device_info.longitude}
        }
        return 'success', 'Success', response
    
    @staticmethod
    def create_story(name, story_type, filename, uploaded_file, **kwargs):
        '''
        :param name:
        :param story_type:
        :param filename:
        :param uploaded_file:
        :param kwargs:
        :return: Creates a story and asynchronously performs actions on it
        '''
        file_path = LocalStorageService.save(filename, uploaded_file)
        story_obj = Story()
        story_obj.name = name
        story_obj.filename = filename
        story_obj.original_path = file_path
        # story_obj._file = ObjectId(str(uuid.uuid4()))
        story_obj.description = kwargs.get('description', '')
        story_obj._type = int(story_type)
        story_obj.created_at = datetime.now()
        story_obj.created_by = kwargs.get('created_by')
        device_info = DeviceInfo(model=kwargs.get('device_info', {}).get('model', ''),
                                 language=kwargs.get('device_info', {}).get('language', 'en'),
                                 user_agent=kwargs.get('device_info', {}).get('user_agent', ''),
                                 versionRelease=kwargs.get('device_info', {}).get('versionRelease', ''),
                                 manufacturer=kwargs.get('device_info', {}).get('manufacturer', ''),
                                 appVersionCode=kwargs.get('device_info', {}).get('appVersionCode', ''),
                                 latitude=kwargs.get('latitude'), longitude=kwargs.get('longitude'),
                                 app_name=kwargs.get('device_info', {}).get('app_name', ''))
        story_obj.device_info = device_info
        story_id = story_obj.save()
        if int(story_type) in [0, 1]:
            invoke_triggers.delay(target='story', action='save', filename=filename,
                                  file_path=file_path, file_type=int(story_type), id=str(story_id.id))
        return 'success', 'Success', {'id': str(story_id.id)}
