import os
import cv2
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from PIL import Image
from bson import ObjectId

from django.conf import settings
from ..models import Story, VideoPath


class SaveFileService(object):
    '''
    Abstract Base class that can be used to have different implementations of storing a file
    '''
    
    @staticmethod
    def save(filename, uploaded_file):
        pass
    

class LocalStorageService(SaveFileService):
    '''
    A class that uses SaveFileService as an Abstract class and overwrites it's implementation of save locally
    '''
    @staticmethod
    def save(filename, uploaded_file):
        '''
        :param filename:
        :param uploaded_file:
        :return: Saves the file in folder specified on local storage
        '''
        path = default_storage.save(settings.BASE_DIR + '/media/' + filename, ContentFile(uploaded_file.read()))
        tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        return tmp_file
        
        
class S3StorageService(SaveFileService):
    '''
    A class that uses SaveFileService as an Abstract class and overwrites it's implementation of save on s3
    '''
    @staticmethod
    def save(filename, uploaded_file):
        '''
        :param filename:
        :param uploaded_file:
        :return: Saves the file on s3
        '''
        pass


class ProcessingFile(object):
        
    def process_file(self, id=None, filename=None, file_path=None, file_type=None):
            '''
            Processes the file on basis of filetype.
            If file is of type text(file_type=2): does nothing
            If file is an image(file_type=0): resized to be max 1200px (height) by 600px (width)
            If file is a video(file_type=1): Changes resolution to 480p
            :param filename:
            :param file_path:
            :param file_type:
            :return: Processes the file as required
            '''
            
            if file_type == 2:
                return None
            elif file_type == 0:
                if os.path.exists(file_path):
                    img = Image.open(file_path)
                    set_width = 600
                    set_height = 1200
                    img = img.resize((set_width, set_height))
                    path_to_set=settings.BASE_DIR + '/media/resized_' + filename
                    path = img.save(settings.BASE_DIR + '/media/resized_' + filename, 'JPEG', optimize=True)
                    Story.objects.filter(id=ObjectId(str(id))).update(resized_image_path=path_to_set)
                    return path
                
            elif file_type == 1:
                cap = cv2.VideoCapture(file_path)
                if not cap.isOpened():
                    print("Error")
                fourcc = cv2.VideoWriter_fourcc(*'MJPG')
                path = settings.BASE_DIR + '/media/resized_' + filename + '.avi'
                out = cv2.VideoWriter(path,
                                      fourcc, 30,
                                      (640, 480))
                while True:
                    ret, frame = cap.read()
                    if ret is True:
                        b = cv2.resize(frame, (640, 480), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
                        out.write(b)
                    else:
                        break
                
                vid_path = VideoPath(medium_resolution=path)
                Story.objects.filter(id=ObjectId(str(id))).update(resized_video_path=vid_path)
                cap.release()
                out.release()
                cv2.destroyAllWindows()
