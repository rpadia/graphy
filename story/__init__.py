from .trigger import Triggers
from .services.file_services import ProcessingFile
p = ProcessingFile()
Triggers.subscribe(target='story', action='save', handler=p.process_file)
print(Triggers)