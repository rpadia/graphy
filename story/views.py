from django.shortcuts import render

# Create your views here.
from rest_framework import status as st, viewsets
from rest_framework.response import Response

from .utils import create_response
from .services.story_services import StoryStorer
from .serializers import StoryGETSerializer


class StoryViewSet(viewsets.ViewSet):
    
    def get(self, request):
        '''
        :param request:
        
        URL: GET http://127.0.0.1:8000/story/
        HEADERS:
            Content-Type: application/json
            Content-Length:
            Host:
            User-Agent:
            Accept:
            Latitude: <Decimal values>
            Longitude: <Decimal values>
        
        :return: Response of list of stories
        {
            "status": "success",
            "status_message": "Success",
            "response": [{}, {}, ...]
        }
        '''
        status, status_message, response = StoryStorer.get_stories()
        return Response(create_response(status, status_message, response))
    
    def retrieve(self, request, pk):
        '''
        :param request:
        :param pk: Story ID
        
        URL: GET http://127.0.0.1:8000/story/<pk>
        HEADERS:
            Content-Type: application/json
            Content-Length:
            Host:
            User-Agent:
            Accept:
            Latitude: <Decimal values>
            Longitude: <Decimal values>
        :return:  {
            "status": "success",
            "status_message": "Success",
            "response": {}
        }
        '''
        if not pk:
            return Response(create_response(status='error',
                                            status_message='Please specify a story',
                                            response={}))
        status, status_message, response = StoryStorer.get_story(story_id=pk)
        serializer = StoryGETSerializer(data=response)
        if not serializer.is_valid():
            return Response(create_response(status='error',
                                            status_message=serializer.errors,
                                            response={}))
        return Response(create_response(status=status,
                                        status_message=status_message,
                                        response=response))
    
    def create(self, request):
        '''
        :param request:
        
        URL: POST http://127.0.0.1:8000/story
        HEADERS:
            Content-Type: application/json
            Content-Length:
            Host:
            User-Agent:
            Accept:
            Latitude: <Decimal values>
            Longitude: <Decimal values>
        :return:  {
            "status": "success",
            "status_message": "Success",
            "response": {}
        }
        '''
        name = request.POST.get('name')
        story_type = request.POST.get('type')
        description = request.POST.get('description')
        created_by = request.POST.get('created_by')
        if not name or not story_type:
            return Response(create_response(status='error',
                                            status_message='Parameters are missing. Please enter name and story type',
                                            response={}), status=st.HTTP_400_BAD_REQUEST)
        device_info = {
            "model": request.META.get("HTTP_MODEL", ''),
            "language": request.META.get("HTTP_LANGUAGE", 'en'),
            "user_agent": request.META.get("HTTP_USER_AGENT", ''),
            "manufacturer": request.META.get("HTTP_MANUFACTURER", ''),
            "versionRelease": request.META.get("HTTP_VERSIONRELEASE", ''),
            "appVersionCode": request.META.get('HTTP_APPVERSIONCODE', ''),
            'latitude': request.META.get('HTTP_LATITUDE', ''),
            'longitude': request.META.get('HTTP_LONGITUDE', ''),
            'app_name': request.META.get('HTTP_APP_NAME', '')
        }
        uploaded_file = request.FILES.get('file')
        filename = request.POST.get('fname')
        if story_type in [0, 1, 2] and not uploaded_file:
            return Response(create_response(
                status='error',
                status_message='Please upload a file',
                response={}
            ))
        status, status_message, response = StoryStorer.create_story(name, story_type, filename, uploaded_file,
                                                                    device_info=device_info, description=description,
                                                                    created_by=created_by)
        return Response(create_response(
            status=status,
            status_message=status_message,
            response=response
        ), status=st.HTTP_200_OK if status == 'success' else st.HTTP_400_BAD_REQUEST)
