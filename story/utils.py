def create_response(status, status_message, response=None):
    if response is None:
        response = {}
    response_json = {
        'status': status.lower(),
        'status_message': status_message,
        'response': response
    }
    return response_json


STORY_TYPE_MAPPER = {
        0: 'Image',
        1: 'Video',
        2: 'Text'
}