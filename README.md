#### Stack Used
* Django 2.2
* Python 3.6+
* MongoDB

#### Project Structure
```bash
    graphy:
       graphy:
          __init__.py
          settings.py
          urls.py
          wsgi.py
       media:
       story:
          __init__.py
          admin.py
          apps.py
          models.py
          views.py
          migrations:
          services:
       utilities:
    graphyVenv:
```

#### Steps to Deploy

* mkdir folder
* cd folder
* Create a virtualenv: virtualenv --python=python3.6 <venv_name>
* activate venv: source <venv_name>/bin/activate
* git clone repo
* cd repo_folder
* Install requirements: pip3 install -r requirements.txt
* Ensure to have set up mongo, enter credentials in settings.py
* Update settings.py with respective details (DB credentials, STATICFILES_DIRS)
* Run Server: python manage.py runserver


#### To setup Celery

* Open a new terminal
* cd into repo folder
* Activate venv
* Run command:  celery -A graphy worker -l info


#### API URLs documented in views.py