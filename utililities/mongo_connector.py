import pymongo
import urllib.parse
from graphy.graphy.config import MongoCredentials


def mongo_client(db_name):
    password = urllib.parse.quote_plus(MongoCredentials().mongo_password)
    uri_string = 'mongodb://' + MongoCredentials().mongo_user + ':' + password + '@' + \
        MongoCredentials().mongo_host + ':' + str(MongoCredentials().mongo_port) + '/ziploan'
    mongo_connection = pymongo.MongoClient(uri_string, connect=False)
    return mongo_connection[db_name]
